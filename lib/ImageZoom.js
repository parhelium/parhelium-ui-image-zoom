var logger = loggerFactory('ImageZoom');
var logger2 = loggerFactory('ImageZoomPreview');

function getAsync(url) {
    return new Promise(function (resolve, reject) {
        var img = new Image();
        img.onload = resolve;
        img.onerror  = reject;
        img.setAttribute("src",url);
    })
}

ImageZoomPreview = Ractive.extend({
    el:'body',
    append:true,
    template:function(){return Ract.getTemplate('ImageZoomPreview')},
    oninit:function(){
        logger2.log('oninit');
        var self = this;

    },
    onrender:function(){
        var self = this;
        ImageZoomPreview.instance = self;
        self.preview = self.find(".imageZoomPreview");
    }
})

ImageZoom = Ractive.extend({
    adapt: ['promise-alt'],
    zoom:4,
    maxZoom:8,
    minZoom:1,
    append: true,
    template: function(){return Ract.getTemplate('ImageZoom')},
    _mouseMoveTimerInterval:50,
    onteardown:function(){
        var self = this;
        clearInterval(self.onMouseMoveTimerTimeout)
    },
    oninit: function () {
        logger.log("oninit", this);
        var self = this;
        self.logger = loggerFactory("ImageZoom " + self._guid );
        if(!ImageZoomPreview.instance){
            new ImageZoomPreview({
                data:self.data
            })
        }

        self.set('preview_width', self.get('preview_width') || 360)
        self.set('preview_height', self.get('preview_height') || 360)
        self.set('onMouseOver', false);
        self.set('onZoomActivated', false);


        self.on({
            onThumbMouseClick:function(e){
                self.set('onMouseOver', false);
                self.set('onZoomActivated', false);
                ImageZoomPreview.instance.data = self.data;
                ImageZoomPreview.instance.update();
            },
            onThumbMouseOver:function(e){
                self.set('onMouseOver', true);

                clearTimeout(self.onMouseOverTimeout);
                self.onMouseOverTimeout = setTimeout(function(){
                    if( self.get('onMouseOver') == false ) return;

                    ImageZoomPreview.instance.data = self.data;
                    ImageZoomPreview.instance.update();
                    var previewPosition =  self.getPreviewPosition();
                    ImageZoomPreview.instance.preview.style.left        = previewPosition.left +"px";
                    ImageZoomPreview.instance.preview.style.top         = previewPosition.top +"px";


                    self.set('imageLoader',getAsync(self.get('source_url')));
                    getAsync(self.get('source_url'))
                        .then(function(evt){
                            self.set('onZoomActivated', true);
                            try{
                                self.logger.log("evt = ",evt);
                                var img;
                                if(evt.path && evt.path[0]) {
                                    img = evt.path[0];
                                }else{
                                    img = evt.target;
                                }
                                var bPreview = ImageZoomPreview.instance.preview.getBoundingClientRect();
                                self.maxZoom = img.width / self.get('preview_width');
//                                logger.log("self.maxZoom", self.maxZoom);
                                if(self.zoom >=  self.maxZoom) self.zoom = self.maxZoom;
                            }catch(e){
                                self.logger.error(e,self);
                            }finally{
                            }
                        });
                },300)
                clearInterval(self.onMouseMoveTimerTimeout)
                self.onMouseMoveTimerTimeout = setInterval(self.onMouseMoveTimer.bind(self),self._mouseMoveTimerInterval)
                self.onMouseMoveTimer();
            },
            onThumbMouseMove:function(e){
                this.mouseMoveEvent = e;
            },
            onThumbMouseOut:function(e){
                this.logger.log("MouseOut");
                self.set('onMouseOver', false)
                self.set('onZoomActivated', false);
                clearInterval(self.onMouseMoveTimerTimeout)

                ImageZoomPreview.instance.data = self.data;
                ImageZoomPreview.instance.update();
                self.set('previewVisibilityClass','imageZoom_off');

            }
        });


    },
    onMouseMoveTimer:function(){
        var self = this;
        var e = self.mouseMoveEvent;
        if(!e) return;

        var percentageX = (e.original.offsetX || e.original.layerX) / ( e.original.target.width || e.original.target.naturalWidth );
        var percentageY = (e.original.offsetY || e.original.layerY) / ( e.original.target.height || e.original.target.naturalHeight);
        var obj = {x:percentageX,y:percentageY}
//        self.logger(obj);

        self.setGlassPosition(obj)
        self.setPreviewImagePosition(obj)
    },

    onrender: function(){
        var self = this;
        self.thumb   = self.find('.imageZoom_container>img');
        self.glass   = self.find('.imageZoom_glass');
    },


    setGlassPosition:function(coords){
        var self= this;
        var style = ""
        if(self.get('onMouseOver')){
            var bThumb = self.thumb.getBoundingClientRect();
            var gWid = Math.floor( bThumb.width  / self.zoom );
            var gHei = Math.floor( bThumb.height / self.zoom );
            var gWidPer = gWid / bThumb.width;
            var gHeiPer = gHei / bThumb.height;
            var x = 0 ;
            var y = 0;

            if(coords.x < gWidPer/2)                     x = 0;
            else if(coords.x > 1 - gWidPer / 2)          x = (1-gWidPer) * bThumb.width;
            else                                         x = (coords.x - gWidPer/2) * bThumb.width;

            if(coords.y < gHeiPer/2)                      y = 0;
            else if(coords.y > 1 - gHeiPer/2)             y = (1 - gHeiPer) * bThumb.height;
            else                                          y = (coords.y - gHeiPer/2) * bThumb.height;

            style   += "left:" + (x) + "px;";
            style   += "top:"  + (y) + "px;"
            style   += "width:" + gWid +"px;";
            style   += "height:" + gHei +"px";
        }
        self.set('glassPosition', style);
    },

    setPreviewImagePosition:function(coords){
        var self= this;
        var style = ""
        if(self.get('onMouseOver')){
            var bPreview = ImageZoomPreview.instance.preview.getBoundingClientRect();
            var imgWid = self.get('preview_width')  * self.zoom;
            var imgHei = self.get('preview_height') * self.zoom;

            var x = -coords.x * (imgWid - self.get('preview_width') );
            var y = -coords.y * (imgHei - self.get('preview_height') );


            style   += "left:" + parseInt(x) + "px;";
            style   += "top:"  + parseInt(y) + "px;"
            style   += "width:" + parseInt(imgWid) +"px;";
            style   += "height:" + parseInt(imgHei) +"px";
        }
        ImageZoomPreview.instance.set('previewImageStyle', style);
    },
    getPreviewPosition:function(){
        var self= this;
        var result = {
            left:0,
            top:0
        };

        var w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName('body')[0],
            x = w.innerWidth || e.clientWidth || g.clientWidth,
            y = w.innerHeight|| e.clientHeight|| g.clientHeight;


        if(self.get('onMouseOver')){
            var bThumb = self.thumb.getBoundingClientRect();
            var bPreview = {width:self.get('preview_width'), height:self.get('preview_height')}
            var margin = 10
            result.left = bThumb.width + margin;
            if(bThumb.left > bPreview.width + margin){
                result.left = parseInt(bThumb.left +( - bPreview.width - margin));
                if(bThumb.top > bPreview.height/2){
                    result.top   =   parseInt(bThumb.top + bThumb.height/2 - bPreview.height/2);
                }else{
                    result.top = bThumb.top;
                }
            }else if(x-bThumb.left > bPreview.width + margin ){
                result.left = parseInt(bThumb.left + bThumb.width + margin );
                if(bThumb.top > bPreview.height/2){
                    result.top   =   parseInt(bThumb.top + bThumb.height/2 - bPreview.height/2);
                }else{
                    result.top = bThumb.top;
                }
            }
        }
        return result
    }
});