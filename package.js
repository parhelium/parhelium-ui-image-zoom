Package.describe({
  name: 'parhelium:ui-image-zoom',
  summary:  "Ractive's component which allows to show image zoom.",
  version: '1.0.0'
});

Package.onUse(function(api) {
    api.versionsFrom('METEOR@0.9.0');
    api.use([
        'less',
        'parhelium:bluebird',
        'parhelium:logger',
        'parhelium:ractive@0.6.0',
        'parhelium:ractive-promise-alt',
        'parhelium:templating-ractive'
    ], ['client']);

    api.imply('parhelium:ractive@0.6.0')
    api.addFiles(
        [
            'lib/ImageZoom.js',
            'lib/ImageZoom.ract',
            'lib/ImageZoomPreview.ract',
            'lib/ImageZoom.less'
        ],
        'client'
    );
    api.export('ImageZoom')
    api.export('ImageZoomPreview')
});
